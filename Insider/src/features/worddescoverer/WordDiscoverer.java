package features.worddescoverer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import features.T_FileAnalyzer;
import files.T_File;
import results.ResultContainer;
import utils.HashMapUtiliter;
import utils.Property;
import utils.json.JsonEncoder;

public class WordDiscoverer implements T_FileAnalyzer
{

	private static HashSet<String> acceptedWords;
	private static List<String> programmingKeyWords;
	private static final String delimiters = "([^a-zA-Z])|([\t\n ])";
	private static final String englishDictionaryLocation = "config" + File.separator + "WordDiscoverer" + File.separator + "words.txt";
	private static final String programmingKeyWordsFileLocation = "config" + File.separator + "WordDiscoverer" + File.separator + "programmingKeyWords.txt";
	
	private static final String allWordsOutputFileName = "results" + File.separator + "AllWords.txt";
	private static final String veryImportantWordsFileName = "results" + File.separator + "MostImportantWords.txt";
	
	private static boolean alreadyConfigured = false;
	
	private HashMap<String, Integer> allWordsMap;
	
	private int maxNumber;
	private boolean onlyEnglish;
	private int minimumAppearance;
	
	private int veryImportantWordLimit;
	private int veryImportantWords;
	
	public void closeFile()
	{
		allWordsMap = (HashMap<String, Integer>) HashMapUtiliter.sortWords(allWordsMap);
		Writer writer = null;
		Writer veryImportantWordsWriter = null;
		try
		{
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(allWordsOutputFileName)));
			veryImportantWordsWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(veryImportantWordsFileName)));
			for(String key:allWordsMap.keySet())
			{
				String s="";
				int app = allWordsMap.get(key);
				if(app>veryImportantWordLimit)
				{
					veryImportantWordsWriter.write(key + "\t" + app + "\n");
					veryImportantWords++;
				}
				for(int i=0; i<app; i++)
				{
					s += key + " ";
				}
//				System.out.println(key);
				writer.write(s + "\n");
			}
			writer.close();
			veryImportantWordsWriter.close();
		}
		catch(IOException e)
		{
			System.out.println("Could not colse file: " + allWordsOutputFileName);
			e.printStackTrace();
		}
	}
	
	private void addWordsToMap(String word, int app)
	{
		if(word.trim().equals(""))
			return;
		
		Integer newapp = 0;
		if((newapp=allWordsMap.get(word))!=null)
		{
			newapp += app;
		}
		else
			newapp = app;
		
		allWordsMap.put(word, newapp);
	}
	
	public WordDiscoverer(int nrOfWords, boolean onlyEnglishWords, int minimumAppearance, int veryImportantWordLimit)
	{
		if(!alreadyConfigured)
		{
			configure();
		}
		this.maxNumber = nrOfWords;
		onlyEnglish = onlyEnglishWords;
		this.minimumAppearance = minimumAppearance;
		this.veryImportantWordLimit = veryImportantWordLimit;
		this.veryImportantWords = 0;
		allWordsMap = new HashMap<>();
	}
	
		private static void configure()
		{
			acceptedWords = new HashSet<>();
			programmingKeyWords = new ArrayList<>();
			alreadyConfigured = true;
			
			String line;
			BufferedReader engDictReader = null;
			BufferedReader keyWordsReader = null;
			try
			{
				engDictReader = new BufferedReader(new FileReader(englishDictionaryLocation));
				while((line = engDictReader.readLine()) != null)
				{
					acceptedWords.add(line);
				}
				
				keyWordsReader = new BufferedReader(new FileReader(programmingKeyWordsFileLocation));
				while((line = keyWordsReader.readLine()) != null)
				{
					programmingKeyWords.add(line);
				}
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				try
				{
					engDictReader.close();
					keyWordsReader.close();
				}
				catch(IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		
	@Override
	public int analyze(T_File file) 
	{
		String content = file.getContent();
		
		String[] words = content.split(delimiters);
		
		List<String> wordList = new LinkedList<String>(Arrays.asList(words));
		
		while(wordList.remove(""));
		
		
		for(int i=0; i<wordList.size(); i++)
		{
			String[] auxArray = wordList.get(i).split("(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])");
			if(auxArray.length > 1)
			{
				wordList.remove(i);
				i--;
				wordList.addAll(Arrays.asList(auxArray));
			}
		}
		
		HashMap<String, Integer> wordAppearance = new HashMap<>();
		
		for(int i=0; i<wordList.size(); i++)
		{
			String word = wordList.get(i).toLowerCase();
			if(word.length() <3 || programmingKeyWords.contains(word))
				continue;
			
			if(wordAppearance.containsKey(word))
			{
				Integer app = wordAppearance.get(word);
				Integer newapp = app + 1;
				wordAppearance.put(word,newapp);
			}
			else
			{
				wordAppearance.put(word, new Integer(1));
			}
		}
		
		if(onlyEnglish)
		{
			Set<String> allKeys = new HashSet<>();
			allKeys.addAll(wordAppearance.keySet());
			for(String key : allKeys)
			{
				if(!acceptedWords.contains(key))
					wordAppearance.remove(key);
			}
		}
		
		wordAppearance = (HashMap<String, Integer>) HashMapUtiliter.sortWords(wordAppearance);
		
		int count = 0;
		String[] mostImportantWords = new String[maxNumber];
		for(String key: wordAppearance.keySet())
		{
			int app = wordAppearance.get(key);
			if(count==maxNumber || app<minimumAppearance)
				break;
			mostImportantWords[count++] = key + "_" + app;
			addWordsToMap(key, app);
		}
		
		Property p = new Property("frequent_words", JsonEncoder.encode(mostImportantWords), "about");
		
		file.addProperty(p);
		
		return 0;
	}

	@Override
	public void generateResults(ResultContainer rc) 
	{
		closeFile();
		rc.writeResult("FrequentWords_" + veryImportantWordLimit + "\t" + veryImportantWords);
	}
	
}
