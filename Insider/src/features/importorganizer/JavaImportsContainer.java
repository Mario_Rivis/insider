package features.importorganizer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import features.importorganizer.datastruct.JavaImportPackage;
import utils.HashMapUtiliter;

public class JavaImportsContainer extends ImportsContainer
{
	private static final String ignoredImportsFileName = "config" + File.separator + "IgnoredImports.txt";
	private static final String importsOutputFileName = "results" + File.separator + "Imports.txt";
	private static final String filesWithImportsName = "results" + File.separator + "FilesWithImports.txt"; 
	private static final String packageImportsOutputFileName = "results" + File.separator + "PackageImports.txt";
	
	private static String[] ignoredImports;
	
	private static HashMap<String, Integer> imports = new HashMap<String, Integer>();
	private static HashMap<String, Integer> filesWithImports = new HashMap<>();
	private static List<JavaImportPackage> packages = new ArrayList<>();

	public static void addImport(String newImport)
	{
		int newapp = 1;
		Integer app = imports.get(newImport);
		if(app!=null)
			newapp = app+1;
		imports.put(newImport, newapp);
	}
	
	public static void writeImportsToFile()
	{
		imports = (HashMap<String,Integer>)HashMapUtiliter.sortWords(imports);
		
		Writer writer = null;
		try
		{
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(importsOutputFileName)));
			for(String importKey: imports.keySet())
			{
				writer.write(importKey + "\t" + imports.get(importKey) + "\n");
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch(IOException e)
			{
				System.out.println("Could not colse file: " + importsOutputFileName);
				e.printStackTrace();
			}
		}
	}
	
	public static void configure()
	{
		BufferedReader reader = null;
		String line = "";
		List<String> ignoredImportsList = new ArrayList<>();
		
		try
		{
			reader = new BufferedReader(new FileReader(ignoredImportsFileName));
			while((line = reader.readLine()) != null)
			{
				ignoredImportsList.add(line);
			}

			ignoredImports = new String[ignoredImportsList.size()];
			ignoredImports = ignoredImportsList.toArray(ignoredImports);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				reader.close();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}

	}
	
	public static boolean isIgnored(String _import)
	{
		for(int i=0; i<ignoredImports.length; i++)
		{
			if(_import.matches(ignoredImports[i]))
				return true;
		}
		
		return false;
	}
	
	public static void addFile(String fileName, int importNo)
	{
		filesWithImports.put(fileName, importNo);
	}
	
	public static void writeFilesWithImportsToFile()
	{
		filesWithImports = (HashMap<String,Integer>)HashMapUtiliter.sortWords(filesWithImports);
		
		Writer writer = null;
		try
		{
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filesWithImportsName)));
			for(String importKey: filesWithImports.keySet())
			{
				writer.write(importKey + "\t" + filesWithImports.get(importKey) + "\n");
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch(IOException e)
			{
				System.out.println("Could not colse file: " + filesWithImportsName);
				e.printStackTrace();
			}
		}
	}
	
	public static void writePackageImportsToFile()
	{
		createPackageList();
		
		Writer writer = null;
		try
		{
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(packageImportsOutputFileName)));
			for(int i=0; i<packages.size(); i++)
			{
				JavaImportPackage _package = packages.get(i);
				writer.write(_package.getName() + "\t" + _package.getFrequency() + "\t" + _package.getClassNo() + "\n");
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch(IOException e)
			{
				System.out.println("Could not colse file: " + packageImportsOutputFileName);
				e.printStackTrace();
			}
		}
	}
	
		private static void createPackageList()
		{
			for(String importKey: imports.keySet())
			{
				String packageName = JavaImportPackage.getPackageName(importKey);
				Integer freq = imports.get(importKey);
				
				JavaImportPackage _package = new JavaImportPackage(packageName,0);
				int index = packages.indexOf(_package);
				if(index==-1)
				{
					_package = new JavaImportPackage(packageName, freq);
					packages.add(_package);
					continue;
				}
				
				_package = packages.get(index);
				_package.increment(freq);
			}
		}
		
	public static int getNrOfFilesWithImports()
	{
		return filesWithImports.size();
	}
	
	public static int getNrOfImportedFiles()
	{
		return imports.size();
	}
	
	public static int getNrOfImports()
	{
		int imp = 0;
		for(String key: imports.keySet())
		{
			imp += imports.get(key);
		}
		
		return imp;
	}
}
