package features.importorganizer.util;

public interface ImportSeparator 
{

	String[] separateImports(String content);
}
