package features.importorganizer.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JavaImportSeparator implements ImportSeparator{

	@Override
	public String[] separateImports(String content) {
		
		List<String> importList = new ArrayList<>();
		
		int firstAcc = content.indexOf("{");
		if(firstAcc==-1)
		{
			System.out.println("file has no class");
			//throw exception to ImportOrganizer to catch
		}
		content = content.substring(0,firstAcc);
		
		Pattern pattern = Pattern.compile("(import [^;]*;)");
		Matcher matcher = pattern.matcher(content);
		while(matcher.find())
		{
			String importString = content.substring(matcher.start()+7, matcher.end()-1).trim();
			
			if(importString.indexOf("static ")==0 || importString.indexOf("static\t")==0)
			{
				importString = importString.substring(6).trim();
			}
			
			importList.add(importString);
		}
		
		String[] importArray = new String[importList.size()];
		importArray = importList.toArray(importArray);
		
		return importArray;
	}

}
