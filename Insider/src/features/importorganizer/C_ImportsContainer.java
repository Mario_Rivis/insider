package features.importorganizer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import features.importorganizer.datastruct.C_Cpp_ObjC_ImportLibrary;
import features.importorganizer.datastruct.JavaImportPackage;
import utils.HashMapUtiliter;

public class C_ImportsContainer extends ImportsContainer
{
	private static final String headersImportedOutputFileName = "results" + File.separator + "Headers.txt";
	private static final String filesWhitHeadersOutputFileName = "results" + File.separator + "FilesWithHeaders.txt"; 
	private static final String librabiesOutputFileName = "results" + File.separator + "Libraries.txt";
	
	private static int ignoredHeaders = 0;
	private static String headersToLibrariesMappingFileName = "config" + File.separator + "headersToLibraries.txt";;
	private static HashMap<String, String> headersToLibraries = new HashMap<>();
	
	private static HashMap<String, Integer> filesWithImports = new HashMap<>();
	private static HashMap<String, Integer> headersImported = new HashMap<>();
	private static List<C_Cpp_ObjC_ImportLibrary> libraries = new ArrayList<>();
	
	
		protected static void configure()
		{
			BufferedReader reader = null;
			String line = "";
			
			String header;
			String library;
			
			
			try
			{
				reader = new BufferedReader(new FileReader(headersToLibrariesMappingFileName));
				while((line = reader.readLine()) != null)
				{
					String[] pair = line.split("\t");
					if(pair.length!=2)
					{
						System.out.println("incorrect input type!!!");
						continue;
						//throw exception
					}
					header = pair[0];
					library = pair[1];
					
					if(headersToLibraries.containsKey(header))
					{
						System.out.println("Header " + header + " present in 2 libraries: " + headersToLibraries.get(header) + " and " + library + "!");
						continue;
						//throw exception
					}
					
					headersToLibraries.put(header, library);
				}
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				try
				{
					reader.close();
				}
				catch(IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		
	public static void addImport(String newHeader)
	{
		int newapp = 1;
		Integer app = headersImported.get(newHeader);
		if(app!=null)
			newapp = app+1;
		headersImported.put(newHeader, newapp);
	}
	
	public static void addFile(String fileName, int importNo)
	{
		filesWithImports.put(fileName, importNo);
	}
	
	public static boolean acceptsHeader(String headerName)
	{
		if( headersToLibraries.containsKey(headerName))
			return true;
		
		ignoredHeaders++;
		return false;
	}
	
	public static int getIgnoredHeaders()
	{
		return ignoredHeaders;
	}
	
	public static void writeImportsToFile()
	{
		headersImported = (HashMap<String,Integer>)HashMapUtiliter.sortWords(headersImported);
		
		Writer writer = null;
		try
		{
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(headersImportedOutputFileName)));
			for(String importKey: headersImported.keySet())
			{
				writer.write(importKey + "\t" + headersImported.get(importKey) + "\n");
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch(IOException e)
			{
				System.out.println("Could not colse file: " + headersImportedOutputFileName);
				e.printStackTrace();
			}
		}
	}
	
	public static void writeFilesWithImportsToFile()
	{
		filesWithImports = (HashMap<String,Integer>)HashMapUtiliter.sortWords(filesWithImports);
		
		Writer writer = null;
		try
		{
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filesWhitHeadersOutputFileName)));
			for(String importKey: filesWithImports.keySet())
			{
				writer.write(importKey + "\t" + filesWithImports.get(importKey) + "\n");
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch(IOException e)
			{
				System.out.println("Could not colse file: " + filesWhitHeadersOutputFileName);
				e.printStackTrace();
			}
		}
	}
	
	public static void writeLibrariesToFile()
	{
		createLibraryList();
		
		Writer writer = null;
		try
		{
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(librabiesOutputFileName)));
			for(int i=0; i<libraries.size(); i++)
			{
				C_Cpp_ObjC_ImportLibrary library = libraries.get(i);
				writer.write(library.getName() + "\t" + library.getFrequency() + "\t" + library.getHeaderNo() + "\n");
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch(IOException e)
			{
				System.out.println("Could not colse file: " + librabiesOutputFileName);
				e.printStackTrace();
			}
		}
	}
	
		private static void createLibraryList()
		{
			for(String headerKey: headersImported.keySet())
			{
				String libraryName = headersToLibraries.get(headerKey);
				Integer freq = headersImported.get(headerKey);
				
				C_Cpp_ObjC_ImportLibrary library = new C_Cpp_ObjC_ImportLibrary(libraryName,0);
				int index = libraries.indexOf(library);
				if(index==-1)
				{
					library = new C_Cpp_ObjC_ImportLibrary(libraryName, freq);
					libraries.add(library);
					continue;
				}
				
				library = libraries.get(index);
				library.increment(freq);
			}
		}
}
