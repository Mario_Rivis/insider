package features.importorganizer.datastruct;

public class JavaImportPackage 
{

	private String name;
	private int frequency;
	private int classNo;
	
	public JavaImportPackage(String name, int freq)
	{
		this.name = name;
		frequency = freq;
		classNo = 1;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String packageName) {
		this.name = packageName;
	}
	public int getFrequency() {
		return frequency;
	}
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	public int getClassNo() {
		return classNo;
	}
	public void setClassNo(int classNo) {
		this.classNo = classNo;
	}
	
	public void increment(int frequeny)
	{
		this.frequency += frequeny;
		this.classNo++;
	}
	
	public boolean equals(Object o)
	{
		if(o instanceof String)
			return ((String)o).equals(name);
		
		if(o instanceof JavaImportPackage)
		{
			return ((JavaImportPackage)o).name.equals(name);
		}
		
		return false;
	}
	
	public static String getPackageName(String _import)
	{
		int index = _import.lastIndexOf('.');
		if(index==-1)
			return _import;
		
		return _import.substring(0, index);
	}
	
//	public static boolean areInSamePackage(String import1, String import2)
//	{
//		int index1 = import1.lastIndexOf('.');
//		int index2 = import2.lastIndexOf('.');
//		
//		if(index1==-1)
//		{
//			if(index2==-1)
//				return import1.equals(import2);
//			
//			return false;
//		}
//		if(index2==-1)
//			return false;
//		
//		String package1 = import1.substring(0, index1);
//		String package2 = import2.substring(0, index2);
//		
//		return package1.equals(package2);
//	}
}
