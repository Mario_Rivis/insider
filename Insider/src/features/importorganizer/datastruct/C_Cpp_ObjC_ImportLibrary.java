package features.importorganizer.datastruct;

import java.util.List;

public class C_Cpp_ObjC_ImportLibrary 
{

	private String name;
	private int headerNo;
	private int frequency;
	
	public C_Cpp_ObjC_ImportLibrary(String name, int frequency) 
	{
		super();
		this.name = name;
		this.frequency = frequency;
		this.headerNo = 1;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHeaderNo() {
		return headerNo;
	}

	public void setHeaderNo(int headerNo) {
		this.headerNo = headerNo;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	
	public void increment(int frequency)
	{
		this.frequency += frequency;
		this.headerNo++;
	}
	
	public boolean equals(Object o)
	{
		if(o instanceof String)
			return ((String)o).equals(name);
		
		if(o instanceof C_Cpp_ObjC_ImportLibrary)
		{
			return ((C_Cpp_ObjC_ImportLibrary)o).name.equals(name);
		}
		
		return false;
	}
	
}
