package features.importorganizer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import features.T_FileAnalyzer;
import features.importorganizer.util.ImportSeparator;
import files.T_File;
import results.ResultContainer;
import utils.FileHelper;

public class ImportOrganizer implements T_FileAnalyzer
{

	private static boolean configured = false;
	
	private ImportSeparator importSeparator;
	private ImportsContainer importsContainer;
	
	public ImportOrganizer()
	{
		if(!configured)
		{
			configured = true;
			JavaImportsContainer.configure();
			C_ImportsContainer.configure();
		}
	}
	
		
	
	public int analyze(T_File file) 
	{
		int importNumber = 0;
		String content = file.getContent();
		
		content = FileHelper.removeComments(content);
		
		if(file.getExtension().equals("java"))
		{
			int firstAcc = content.indexOf("{");
			if(firstAcc==-1)
			{
				System.out.println(file.getName() + "This file has no class");
				return 0;
			}
			content = content.substring(0,firstAcc);
			
			Pattern pattern = Pattern.compile("(import [^;]*;)");
			Matcher matcher = pattern.matcher(content);
			while(matcher.find())
			{
				String importString = content.substring(matcher.start()+7, matcher.end()-1).trim();
				
				if(importString.indexOf("static ")==0 || importString.indexOf("static\t")==0)
				{
					importString = importString.substring(6).trim();
				}
				
				if(!JavaImportsContainer.isIgnored(importString))
				{
					JavaImportsContainer.addImport(importString);
					importNumber++;
				}
			}
		}
		else if(file.getExtension().equals("m")||file.getExtension().equals("mm")||file.getExtension().equals("h")||file.getExtension().equals("cpp")||file.getExtension().equals("c"))
		{
			Pattern pattern = Pattern.compile("(#include[ \t]*<[^>]*>)");
			Matcher matcher = pattern.matcher(content);
			while(matcher.find())
			{
				String importString = content.substring(matcher.start(), matcher.end());
				importString = importString.substring(importString.indexOf('<')+1, importString.indexOf('>'));
				int index = importString.lastIndexOf('/');
				index++;
				importString = importString.substring(index);
				
				if(C_ImportsContainer.acceptsHeader(importString))
				{
					C_ImportsContainer.addImport(importString);
					importNumber++;
					
				}
			}
			
			pattern = Pattern.compile("(#include[ \t]*\"[^\"]*\")");
			matcher = pattern.matcher(content);
			while(matcher.find())
			{
				String importString = content.substring(matcher.start(), matcher.end());
				int firstIndex = importString.indexOf('\"');
				int nextIndex = importString.indexOf('\"', firstIndex+1);
				importString = importString.substring(firstIndex+1, nextIndex);
				int index = importString.lastIndexOf('/');
				index++;
				importString = importString.substring(index);
				
				if(C_ImportsContainer.acceptsHeader(importString))
				{
					C_ImportsContainer.addImport(importString);
					importNumber++;
				}
			}
			
			pattern = Pattern.compile("(#import[ \t]*\"[^\"]*\")");
			matcher = pattern.matcher(content);
			while(matcher.find())
			{
				String importString = content.substring(matcher.start(), matcher.end());
				int firstIndex = importString.indexOf('\"');
				int nextIndex = importString.indexOf('\"', firstIndex+1);
				importString = importString.substring(firstIndex+1, nextIndex);
				int index = importString.lastIndexOf('/');
				index++;
				importString = importString.substring(index);
				
				if(C_ImportsContainer.acceptsHeader(importString))
				{
					C_ImportsContainer.addImport(importString);
					importNumber++;
				}
			}
			
			pattern = Pattern.compile("(#import[ \t]*<[^>]*>)");
			matcher = pattern.matcher(content);
			while(matcher.find())
			{
				String importString = content.substring(matcher.start(), matcher.end());
				importString = importString.substring(importString.indexOf('<')+1, importString.indexOf('>'));
				int index = importString.lastIndexOf('/');
				index++;
				importString = importString.substring(index);
				
				if(C_ImportsContainer.acceptsHeader(importString))
				{
					C_ImportsContainer.addImport(importString);
					importNumber++;
				}
			}
		}
//		String[] imports = importSeparator.
			
		if(importNumber>0)
		{
			if(file.getExtension().equals(".java"))
				JavaImportsContainer.addFile(file.getName(), importNumber);
			else if(file.getExtension().equals("m")||file.getExtension().equals("mm")||file.getExtension().equals("h")||file.getExtension().equals("cpp")||file.getExtension().equals("c"))
				C_ImportsContainer.addFile(file.getName(), importNumber);
		}
		return importNumber;
	}



	@Override
	public void generateResults(ResultContainer rc) 
	{		
		JavaImportsContainer.writeImportsToFile();
		JavaImportsContainer.writeFilesWithImportsToFile();
		JavaImportsContainer.writePackageImportsToFile();
		
		C_ImportsContainer.writeImportsToFile();
		C_ImportsContainer.writeFilesWithImportsToFile();
		C_ImportsContainer.writeLibrariesToFile();
		
		rc.writeResult("PackageImports\t" + JavaImportsContainer.getNrOfFilesWithImports() + "\t" + JavaImportsContainer.getNrOfImportedFiles() + "\t" + JavaImportsContainer.getNrOfImports());
	}

}
