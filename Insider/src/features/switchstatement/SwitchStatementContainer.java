package features.switchstatement;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class SwitchStatementContainer 
{
	private static List<SwitchStatementDataStruct> switchList = new ArrayList<>();
	
	private static int nrOfCases = 0;
	
	public static void add(SwitchStatementDataStruct s)
	{
		switchList.add(s);
		nrOfCases += s.getCases().length;
	}
	
	
	public static void setNrOfCases(int nrOfCases) {
		SwitchStatementContainer.nrOfCases = nrOfCases;
	}

	public static void writeOutputFile(String fileName)
	{
		Writer writer = null;
		try
		{
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName)));
			for(int i=0; i<switchList.size(); i++)
			{
				SwitchStatementDataStruct switchStruct = switchList.get(i);
				writer.write(switchStruct.getFileName());
				writer.write(" " + switchStruct.getStartLine() + " " + switchStruct.getStopLine());
				writer.write("\n");
				
				String[] cases = switchStruct.getCases();
				for(int j=0; j<cases.length; j++)
				{
					writer.write("\t" + cases[j] + "\n");
				}
				
				writer.write("\n\n");
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch(IOException e)
			{
				System.out.println("Could not colse file: " + fileName);
				e.printStackTrace();
			}
		}
	}
	
	public static void writeTabOutputFile(String fileName)
	{
		Writer writer = null;
		try
		{
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName)));
			for(int i=0; i<switchList.size(); i++)
			{
				SwitchStatementDataStruct switchStruct = switchList.get(i);
				
				String[] cases = switchStruct.getCases();
				for(int j=0; j<cases.length; j++)
				{
					writer.write(switchStruct.getFileName() + "\t");
					writer.write(SwitchFinder.extractCaseValue(cases[j]) + "\t");
					writer.write(switchStruct.getStartLine() + "_" + switchStruct.getStopLine() + "\t");
					writer.write((j+1) + "\n");
				}
				
				writer.write("\n\n");
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch(IOException e)
			{
				System.out.println("Could not colse file: " + fileName);
				e.printStackTrace();
			}
		}
	}
	
	protected static int getNrOfCases() {
		return nrOfCases;
	}

	
}
