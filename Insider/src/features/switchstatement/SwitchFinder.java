package features.switchstatement;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import features.T_FileAnalyzer;
import files.T_File;
import results.ResultContainer;
import utils.FileHelper;
import utils.Property;

public class SwitchFinder implements T_FileAnalyzer 
{	
	private static final String switchRegex = "([\n\t ](switch)[\t ]*\\()";
	private static final String caseRegex = "([\n\t ](case) )";
	
	private String standardOutputFileName;
	private String standatdTabSwitchOutputFileName;
	
	private static T_File file;

	private int nrOfFilesWithSwitches = 0;
	
	public SwitchFinder(String standardOutputFileName, String standatdTabSwitchOutputFileName) {
		super();
		this.standardOutputFileName = standardOutputFileName;
		this.standatdTabSwitchOutputFileName = standatdTabSwitchOutputFileName;
	}

	private static String removeSubstring(String s, int startIndex, int stopIndex)
	{
		String ret = s.substring(0,startIndex);
		ret += s.substring(stopIndex+1);
		
		return ret;
	}
	
	private static String removeUnwantedPortion(String switchString)
	{
		String ret = switchString;
		
		int indexFrom = switchString.indexOf('{');
		int openAcc;
		if((openAcc = switchString.indexOf('{', indexFrom+1))!=-1)
		{
			int matchAcc = findMatchingAcc(ret,openAcc+1);
			ret = removeSubstring(ret, openAcc, matchAcc);
			ret = removeUnwantedPortion(ret);
		}
		
		return ret;
	}
	
	private static int findMatchingAcc(String s, int fromIndex)
	{
		char[] charArray = s.toCharArray();
		int len = s.length();
		int open = 1;
		for(int i=fromIndex; i<len; i++)
		{
			if(charArray[i]=='{')
				open++;
			else if(charArray[i]=='}')
				open--;
			
			if(open==0)
			{
				s = Arrays.toString(charArray);
				return i;
			}
				
		}
		
		return -1;
	}
	
	private static String[] extractCases(String switchString)
	{
		List<String> cases = new ArrayList<String>();
		Pattern p = Pattern.compile(caseRegex);
		Matcher matcher  = p.matcher(switchString);
		int caseIndex;
		String caseValue;
		
 		while(matcher.find())
		{
			caseIndex = matcher.start();
			
			if(isInString(caseIndex, switchString))
			{
				continue;
			}
			
			int colonIndex = switchString.indexOf(':', caseIndex);
			
			if(colonIndex >= 0)
			{
				caseValue = switchString.substring(caseIndex+1, colonIndex);
				cases.add(caseValue);
			}
			else
			{
				System.out.println(file.getName() + "\n" + switchString);
			}
			
			
		}
		
		String[] returnArray = new String[cases.size()];
		returnArray = cases.toArray(returnArray);
		
		return returnArray;
	}
	
	private static int extractSwitches(String s)
	{
		int switches = 0;
		Pattern p = Pattern.compile(switchRegex);
		Matcher matcher = p.matcher(s);
		
		int indexSwitch;
		int fromIndex = 0;
		while(matcher.find())
		{
			indexSwitch = matcher.start();
			
			if(isInString(file.getIndexRelativeToLine(indexSwitch), file.getLineWithIndex(indexSwitch)))
			{
				fromIndex = indexSwitch + 6;
				continue;
			}
				
			int firstIndex = s.indexOf('{', indexSwitch);
			int lastIndex = findMatchingAcc(s, firstIndex+1);
		
			String switchString = s.substring(indexSwitch, lastIndex+1);
			
			switchString  = removeUnwantedPortion(switchString);
			
			String[] cases = extractCases(switchString);
			
			SwitchStatementDataStruct switchStruct = new SwitchStatementDataStruct(cases, file.getLineFromIndex(indexSwitch), file.getLineFromIndex(lastIndex), file.getName());
			
			SwitchStatementContainer.add(switchStruct);
			
			fromIndex = indexSwitch + 6;
			
			switches++;
		}
		
		
		return switches;
	}
	
	private static boolean isInString(int index, String switchString)
	{
		int nrQoutes = 0;
		char[] letters = switchString.toCharArray();
		for(int i=index; i>=0; i--)
		{
			if(letters[i]=='\n')
				break;
			if(letters[i] == '\"')
				nrQoutes++;
		}
		
		if(nrQoutes%2==1)
			return true;
		
		return false;
	}

	public int analyze(T_File t_file) 
	{
		file = t_file;
		String content = file.getContent();

		//System.out.println(t_file.getName());
		content = FileHelper.removeComments(content);
		
		int switches = extractSwitches(content);
		
		if(switches==0)
			return 0;
		
		nrOfFilesWithSwitches++;
		Property p = new Property("number_of_switches", new Integer(switches), "switchStatements");
		
		file.addProperty(p);
		return switches;
	}
	
	protected static String extractCaseValue(String caseStatement)
	{
		String aux = caseStatement.substring(caseStatement.indexOf("case") + 4);
		String ret = "";
		char[] auxLetters = aux.toCharArray();
		
		int i=0;
		for(i=0; i<aux.length(); i++)
		{
			if(!Character.isWhitespace(auxLetters[i]))
				break;
		}
		
		if(auxLetters[i]=='\"')
		{
			ret = "\"";
			for(int j=i+1;j<auxLetters.length; j++)
			{
				if(auxLetters[j]=='\"')
				{
					ret += auxLetters[j];
					return ret;
				}
				
				ret += auxLetters[j];
			}
			
			return null;
		}
		
		for(int j=i;j<auxLetters.length; j++)
		{
			if(Character.isWhitespace(auxLetters[j]))
			{
				return ret;
			}
			ret += auxLetters[j];
		}
		return ret;
	}

	@Override
	public void generateResults(ResultContainer rc) 
	{
		SwitchStatementContainer.writeOutputFile(standardOutputFileName);
		SwitchStatementContainer.writeTabOutputFile(standatdTabSwitchOutputFileName);
		rc.writeResult("Switch\t" + nrOfFilesWithSwitches + "\t" + SwitchStatementContainer.getNrOfCases());
	}

}
