package features.switchstatement;

public class SwitchStatementDataStruct {
	
	private String[] cases;
	private int startLine;
	private int stopLine;
	private String fileName;
	
	public SwitchStatementDataStruct(String[] cases, int startLine, int stopLine, String fileName) 
	{
		super();
		this.cases = cases;
		this.startLine = startLine;
		this.stopLine = stopLine;
		this.fileName = fileName;
	}
	
	public String[] getCases() {
		return cases;
	}

	public void setCases(String[] cases) {
		this.cases = cases;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getStartLine() {
		return startLine;
	}
	
	public void setStartLine(int startLine) {
		this.startLine = startLine;
	}
	
	public int getStopLine() {
		return stopLine;
	}
	
	public void setStopLine(int stopLine) {
		this.stopLine = stopLine;
	}
	
	
}
