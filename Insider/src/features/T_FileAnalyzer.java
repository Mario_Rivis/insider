package features;

import files.T_File;
import results.ResultContainer;

public interface T_FileAnalyzer {

	int analyze(T_File file);
	void generateResults(ResultContainer rc);
}
