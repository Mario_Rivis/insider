package features.indentcounter;

import features.T_FileAnalyzer;
import files.T_File;
import results.ResultContainer;
import utils.Property;

public class IndentCounter implements T_FileAnalyzer
{
	public IndentCounter()
	{}

	@Override
	public int analyze(T_File file) 
	{
		String line;
		int totalIndentation = 0;
		int maxIndentation = 0;
		
		while(file.hasNext())
		{
			line = file.nextLine();
			int indentation = 0;
			char[] letters = line.toCharArray();
			for(int i=0; i<letters.length; i++)
			{
				if(Character.isWhitespace(letters[i]))
				{
					if(letters[i] == '\t')
						indentation += 4;
					else if(letters[i]==' ')
						indentation += 1;
				}
				else
					break;
			}
			
			maxIndentation = Math.max(maxIndentation, indentation);
			totalIndentation += indentation;
		}
		
		Property p1 = new Property("max_indentation", new Integer(maxIndentation/4), "indentation");
		Property p2 = new Property("median_indentation", new Double(totalIndentation/(4*file.getNumberOfLines())), "indentation");
		
		file.addProperty(p1);
		file.addProperty(p2);
		
		return maxIndentation;
	}

	@Override
	public void generateResults(ResultContainer rc) 
	{
		
	}
}
