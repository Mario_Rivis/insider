package features.technlogysearcher;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import features.T_FileAnalyzer;
import files.T_File;
import results.ResultContainer;
import utils.AllTypeFileReader;
import utils.FileHelper;

public class TechnologyManager implements T_FileAnalyzer
{

	private List<Language> languages;
	
	private String folder;
	
	public TechnologyManager(String Root)
	{
		folder = Root;
		languages = new ArrayList<>();
		initialize();
	}
	
	private void initialize()
	{
		List<Technology> techForAll = new ArrayList<>();
		File root = new File(folder);
		
		File[] files = root.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				Language l = new Language(file);
				languages.add(l);
			} else if(FileHelper.getExtension(file.getName()).equals("technology")) {
				Technology t = AllTypeFileReader.readTechnologyFile(file);
				techForAll.add(t);
			}
		}
		
		for(int i=0; i<techForAll.size(); i++)
		{
			for(int j=0; j<languages.size(); j++)
				languages.get(j).addTechnology(techForAll.get(i));
		}
	}
	
	public String toString()
	{
		String ret = "";
		
		for(int i=0; i<languages.size(); i++)
		{
			ret += languages.get(i).toString();
			ret += "\n";
		}
		
		return ret;
	}
	
	public List<Language> getLanguages()
	{
		return languages;
	}

	@Override
	public int analyze(T_File file) 
	{
		if(languages==null)
			return 0;
		
		int ret = 0;
		for(int i=0; i<languages.size(); i++)
			ret += languages.get(i).analyze(file);
		
		return ret;
	}

	@Override
	public void generateResults(ResultContainer rc) 
	{
		if(languages==null)
			return;
		
		for(Language l: languages)
		{
			l.generateResults(rc);
		}
	}
	
	
}
