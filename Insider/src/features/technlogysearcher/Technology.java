package features.technlogysearcher;

import features.T_FileAnalyzer;
import files.T_File;
import results.ResultContainer;

public class Technology implements T_FileAnalyzer{

	private String name;
	private Category[] categories;
	private boolean generatedResults;
	
	public Technology(String name)
	{
		this.name = name;
		generatedResults = false;
	}
	
	public void setCategories(Category[] categories) 
	{
		this.categories = categories;
	}

	public String toString()
	{
		String ret = "Technology: " + name;
			
		return ret;
	}
	
	public String getName()
	{
		return name;
	}

	@Override
	public int analyze(T_File file) 
	{
		int ret = 0;
		if(categories==null)
			return 0;
		for(int i=0; i<categories.length; i++)
		{
			ret += categories[i].analyze(file);
		}
		
		return ret;
	}


	@Override
	public void generateResults(ResultContainer rc) 
	{
		if(categories==null)
			return;
		
		if(generatedResults)
			return;
		
		generatedResults = true;
		
		for(Category c:categories)
			c.generateResults(rc);
	}
}
