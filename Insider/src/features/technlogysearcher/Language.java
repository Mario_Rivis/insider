package features.technlogysearcher;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import features.T_FileAnalyzer;
import files.T_File;
import results.ResultContainer;
import utils.AllTypeFileReader;
import utils.FileHelper;

public class Language implements T_FileAnalyzer{
	
	private String name;
	
	private String[] extensions;
	private List<Technology> technologies;
	
	public Language(File file)
	{
		this.name = file.getName();
		technologies = new ArrayList<>();
		configure(file);
	}
	
	public void configure(File root)
	{
		File[] files = root.listFiles();
		for (File file : files) 
		{
			if(FileHelper.getExtension(file.getName()).equals("extension"))
			{
				extensions = AllTypeFileReader.readExtensionsFile(file);
			}
			else if(FileHelper.getExtension(file.getName()).equals("technology")) 
			{
				technologies.add(AllTypeFileReader.readTechnologyFile(file));
			}
		}
	}

	public void addTechnology(Technology technology) 
	{
		technologies.add(technology);
	}
	
	public String toString()
	{
		return "Language: " + name;
	}

	private boolean suitsExtension(T_File file)
	{
		String fileExt = file.getExtension();
		
		return suitsExtension(fileExt);
	}
	private boolean suitsExtension(String ext)
	{
		return Arrays.asList(extensions).contains(ext);
	}

	@Override
	public int analyze(T_File file) 
	{
		if(technologies==null)
			return 0;
		
		if(!suitsExtension(file))
			return 0;
		
		int ret=0;
		for(int i=0; i<technologies.size(); i++)
		{
			ret += technologies.get(i).analyze(file);
		}
		
		return ret;
	}

	@Override
	public void generateResults(ResultContainer rc) 
	{
		if(technologies==null)
			return;
		
		for(Technology t:technologies)
			t.generateResults(rc);
	}
	
}
