package features.technlogysearcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import features.T_FileAnalyzer;
import files.T_File;
import results.ResultContainer;

public class FingerPrint implements T_FileAnalyzer{

	private String regex;
	
	private int occurences;
	private Category category;
	
	public FingerPrint()
	{
		this.occurences = 0;
	}
	
	public FingerPrint(Category category, String regex)
	{
		this();
		this.category = category;
		this.regex = regex;
	}
	
	public String toString()
	{
		String ret = "";
		
		return ret;
	}
	
	public String getRegex()
	{
		return regex;
	}
		
	public String formatName()
	{
		String ret = "";
		
		ret += "\nDomain: " + category.getName();
		
		ret += "\nRegex: " + regex;
				
		return ret;
	}

	@Override
	public int analyze(T_File file) 
	{
		int fileOcc = 0;
		
		String fileContent = file.getContent();
		
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(fileContent);
		
		while(matcher.find())
		{
			fileOcc++;
		}
		
		occurences += fileOcc;
		
		return fileOcc;
	}

	@Override
	public void generateResults(ResultContainer rc) {
		// TODO Auto-generated method stub
		
	}
	
}
