package features.technlogysearcher;

import features.T_FileAnalyzer;
import files.T_File;
import results.ResultContainer;
import utils.Property;

public class Category implements T_FileAnalyzer{

	private FingerPrint[] fingerPrints;
	private String categoryName;
	private Technology technology;
	private int occurances;
	private int filesFoundIn;
	
	public Category(String name, Technology technology)
	{
		this.technology = technology;
		this.categoryName = technology.getName() + "_" + name;
		occurances=0;
		filesFoundIn = 0;
	}
	
	public void setFingerPrints(FingerPrint[] fingerPrints) 
	{
		this.fingerPrints = fingerPrints;
	}

	public String getName()
	{
		return categoryName;
	}
	
	public void incrementOccurances()
	{
		occurances++;
	}
	
	public String toString()
	{
		String ret = categoryName + "\n";
		
		return ret;
	}

	@Override
	public int analyze(T_File file) 
	{
		int fileOcc = 0;
		if(fingerPrints==null)
			return 0;
		for(int i=0; i<fingerPrints.length; i++)
		{
			fileOcc += fingerPrints[i].analyze(file);
		}
		
		if(fileOcc==0)
			return 0;
		
		filesFoundIn++;
		occurances += fileOcc;
		
		Property p = new Property(this.categoryName, new Integer(fileOcc),  this.technology.getName());
		file.addProperty(p);
		
		return fileOcc;
	}

	@Override
	public void generateResults(ResultContainer rc) 
	{
		if(filesFoundIn==0)
			return;
		
		rc.writeResult(categoryName + "\t" + filesFoundIn + "\t" + occurances);	
	}
}
