package files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import utils.FileHelper;
import utils.Property;
import utils.json.JsonEncoder;

public class T_File {

	private File file;
	private String content;
	private Integer[] lineIndexes;
	private int lines;
	
	private T_FileManager manager;
	
	private int currentLine=0;
	
	private List<Property> properties;
	
	
	public T_File(File file, T_FileManager manager)
	{
		this.file = file;
		this.manager = manager;
		initialize();
		configure();
	}
	
		private void initialize()
		{
			content = "";
			lines = 0;
			properties = new ArrayList<>();
		}
		
		private void configure()
		{
			BufferedReader in = null;
			String line;
			ArrayList<Integer> lineIndexList = new ArrayList<>();
			lineIndexList.add(0);
			int lastIndex = 0;
			try
			{
				in = new BufferedReader(new FileReader(file));
				while((line = in.readLine()) != null)
				{
					content += line;
					content += "\n";
					lineIndexList.add(lastIndex + line.length() + 1);
					lastIndex += line.length()+1;
				}
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
			
			lines = lineIndexList.size();
			lineIndexes = new Integer[lines];
			lineIndexes = lineIndexList.toArray(lineIndexes);
			
		}
	
	public boolean hasNext()
	{
		if(currentLine==lines-1)
		{
			currentLine = 0;
			return false;
		}
		
		return true;
	}
	
	public String nextLine()
	{
		return getLine(currentLine++);
	}
		
		private String getLine(int lineIndex)
		{
			return content.substring(lineIndexes[lineIndex], lineIndexes[lineIndex+1]);
		}
		
	public int getLineFromIndex(int index)
	{
		for(int i=0; i<lines; i++)
		{
			if(index<lineIndexes[i])
				return i;
		}
		
		return -1;
	}
	
	public String getLineWithIndex(int index)
	{
		for(int i=1; i<lineIndexes.length; i++)
		{
			if(index < lineIndexes[i])
				return content.substring(lineIndexes[i-1], lineIndexes[i]);
		}
		
		return "";
	}
	
	public int getIndexRelativeToLine(int index)
	{
		for(int i=1; i<lineIndexes.length; i++)
		{
			if(index < lineIndexes[i])
				return index - lineIndexes[i-1];
		}
		
		return -1;
	}
	
	public String getContent()
	{
		return this.content;
	}
	
	public boolean equals(Object o)
	{
		if(o instanceof T_File)
			return ((T_File)o).file.equals(this.file);
		
		return false;
	}
	
	public String getExtension()
	{
		return FileHelper.getExtension(file.getName());
	}
	
	public String toString()
	{
		String ret = file.getName();
		
		return ret;
	}
	
	public void addProperty(Property p)
	{
		properties.add(p);
	}
	
	public String getName()
	{
		return FileHelper.getRelativeFileName(file, manager.getRootDirectory());
	}

	public void encodeToJSON() 
	{
		JsonEncoder.encode(this, properties);
	}
	
	public int getNumberOfLines()
	{
		return lines;
	}
}
