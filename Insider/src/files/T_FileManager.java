package files;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import features.T_FileAnalyzer;
import results.ResultContainer;
import utils.AllTypeFileReader;
import utils.FileHelper;
import utils.exceptions.IllegalExtensionException;

public class T_FileManager {
	
	private String directoryName;
	private File directory;
	private T_File[] files;
	private static String[] acceptedExtensions;
	
	public T_FileManager(String directoryName) throws FileNotFoundException
	{
		this.directoryName = directoryName;
		initialize();
		configure();
	}
	
		private void initialize() throws FileNotFoundException, IllegalExtensionException
		{
			try
			{
				directory = new File(directoryName);
				if(!directory.exists() && !directory.isDirectory())
					throw new FileNotFoundException("The specified root directory does not exist or is not a directory!\nPlease enter a valid root directory for the project you want to evaluate!");
			}
			catch(NullPointerException e)
			{
				throw new FileNotFoundException("No root folder selected!");
			}
			
			String acceptedExtensionFile = "config" + File.separator + "AcceptedExtensions.extension";

			acceptedExtensions = AllTypeFileReader.readExtensionsFile(acceptedExtensionFile);
		}
		
		private void configure()
		{
			List<T_File> fileList = new ArrayList<>();
			
			searchFilesToProcess(directory, fileList);
			
			files = new T_File[fileList.size()];
			files = fileList.toArray(files);
		}
			
			private void searchFilesToProcess(File dir, List<T_File> filesToProcess) 
			{
				File[] files = dir.listFiles();
				for (File file : files) 
				{
					if (file.isDirectory()) 
					{
						searchFilesToProcess(file,filesToProcess);
					} 
					else if(Arrays.asList(acceptedExtensions).contains(FileHelper.getExtension(file.getName()))) 
					{
						filesToProcess.add(new T_File(file, this));
					}
				}
			}
	
	public String toString()
	{
		String ret = "";
		
		for(int i=0; i<files.length; i++)
		{
			ret += files[i].toString() + "\n";
		}
		
		return ret;
	}
	
	public void encodeAllFilesToJSON()
	{
		for(int i=0; i<files.length; i++)
			files[i].encodeToJSON();
	}
	
	protected File getRootDirectory()
	{
		return directory;
	}
	
	public void analyzeFeature(T_FileAnalyzer feature, ResultContainer rc)
	{
		for(int i=0; i<files.length; i++)
			feature.analyze(files[i]);
		
		feature.generateResults(rc);
	}
}
