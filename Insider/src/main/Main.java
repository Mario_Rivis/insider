package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;

import features.importorganizer.C_ImportsContainer;
import features.importorganizer.ImportOrganizer;
import features.indentcounter.IndentCounter;
import features.switchstatement.SwitchFinder;
import features.technlogysearcher.TechnologyManager;
import features.worddescoverer.WordDiscoverer;
import files.T_FileManager;
import results.ResultContainer;
import utils.exceptions.IllegalExtensionException;
import utils.json.JSONObjectManager;

public class Main {
	
	private static String rootDirectoryName = "E:\\laptop\\projects\\ToTestInsider\\objc4-master";
//	private static String rootDirectoryName = "E:\\laptop\\projects\\ToTestInsider\\argouml";
	private static final String mainTechnologyFolder = "config" + File.separator + "Technologies";
	private static final String C_HEADERS_TO_LIBRARIES_MAPPING = "config" + File.separator + "headersToLibraries.txt";
	
	private static String outputDirectoryName = "results";
	private static String outputFileName = outputDirectoryName + File.separator + "output.json";
	private static String standardOutputFileName = outputDirectoryName + File.separator + "switches.txt";
	private static String standatdTabSwitchOutputFileName = outputDirectoryName + File.separator + "switches_tab.txt";
	
	private static String summaryFileName = outputDirectoryName + File.separator + "Summary.txt";
	
	private static boolean executeWordDiscoverer = false;
	private static boolean executeIndentCounter = true;
	private static boolean executeSwitchFinder = false;
	private static boolean executeImportOrganizer = true;
	
	private static int wordNumber = 10;
	private static boolean onlyEnglish = true;
	private static int minimumNrOfAppearance = 5;
	private static int veryImportantWordsAppearanceLimit = 100;

	public static void main(String[] args) {
		
		
//		if(!verifyConditions(args)) 
//		{
//			System.out.println("\t\tInsider -h or --help for help");
//			return;
//		}
		
		try {
			System.out.println("Searching for files...");
			T_FileManager fileManager = new T_FileManager(rootDirectoryName);
			
			System.out.println("Initializing program...");
			TechnologyManager techManager = new TechnologyManager(mainTechnologyFolder);
			WordDiscoverer wordDiscoverer = new WordDiscoverer(wordNumber, onlyEnglish, minimumNrOfAppearance, veryImportantWordsAppearanceLimit);
			IndentCounter indentCounter = new IndentCounter();
			SwitchFinder switchFinder = new SwitchFinder(standardOutputFileName, standatdTabSwitchOutputFileName);
			ImportOrganizer importOrganizer = new ImportOrganizer();
			
			ResultContainer resultContainer = new ResultContainer(summaryFileName);
			
			System.out.println("TechnologyDigger is analyzing your files...");
			fileManager.analyzeFeature(techManager, resultContainer);
			
			if(executeWordDiscoverer)
			{
				System.out.println("WordDiscoverer is searching for keywords...");
				fileManager.analyzeFeature(wordDiscoverer, resultContainer);
			}
			if(executeIndentCounter)
			{
				System.out.println("IndentCounter is analyzing your files...");
				fileManager.analyzeFeature(indentCounter, resultContainer);
			}
			if(executeSwitchFinder)
			{
				System.out.println("SwitchFInder is searching for all the switches...");
				fileManager.analyzeFeature(switchFinder, resultContainer);
			}
			
			if(executeImportOrganizer)
			{
				System.out.println("ImportOrganizer is searching and organizing your imports...");
				fileManager.analyzeFeature(importOrganizer, resultContainer);
			}
			
			fileManager.encodeAllFilesToJSON();
			JSONObjectManager.printToFile(outputFileName);
			resultContainer.closeFile();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IllegalExtensionException e)
		{
			e.printStackTrace();
		}
		System.out.println(C_ImportsContainer.getIgnoredHeaders());
		System.out.println("Program finished");
	
	}
	
	//check if Insider call is well formed
	private static boolean verifyConditions(String[] args)
	{
		if(args.length==0)
			return false;
		
		if(args[0].equals("-h") || args[0].equals("--help"))
		{
			System.out.println("Insider <Root_of_project>");
			System.out.println("\n\tuses built in technologies to check your project with all the options");
			//System.out.println("\n\nInsider <Root_of_technologies> <Root_of_project>");
			//System.out.println("\n\tuses your own technology foler to check your project");
			//System.out.println("\n\nInsider <Root_of_technologies> <Root_of_project> <output_file>");
			//System.out.println("\n\tuses your own technology foler to check your project and outputs the result in the specified file");
			
			System.out.println("Options: \n\n");
			System.out.println("\t\t -w N or --words N -> executes the Feature of finding the most frequent N words in the english dictionary\n");
			System.out.println("\t\t -wA N or --wordAll -> executes the Fecture of finding the most frequent N words\n");
			
			System.out.println("\t\t -o <OutputDirectory> or --output <OutputDirectory> -> changes the output directory to the specified directory\n");
			
			System.out.println("\t\t -i or --indent -> executes the Feature of calculating the maximal and median indentation for every file in the project\n");
			
			System.out.println("\t\t -s or --switch -> executes the Feature of finding all the switch statements in all the files in the project\n");
			
			return false;
		}
		
		if(args.length==1)
		{
			rootDirectoryName = args[0];
			return true;
		}
		
		rootDirectoryName = args[0];
		
		if(args[1].contains("-"))
		{
			executeIndentCounter = false;
			executeSwitchFinder = false;
			executeWordDiscoverer = false;
		}
		
		for(int i=1; i<args.length; i++)
		{
			switch(args[i])
			{
				case "-w":
				case "--words":
					if(Arrays.asList(args).contains("-wa") || Arrays.asList(args).contains(("--wordAll")))
							return false;
					executeWordDiscoverer = true;
					if(i+1>args.length)
						return false;
					try
					{
						wordNumber = Integer.parseInt(args[i++]);
					}
					catch(NumberFormatException e)
					{
						return false;
					}
					break;
				
				case "-wa":
				case "--wordAll":
					if(Arrays.asList(args).contains("-wa") || Arrays.asList(args).contains(("--wordAll")))
							return false;
					executeWordDiscoverer = true;
					if(i+1>args.length)
						return false;
					try
					{
						wordNumber = Integer.parseInt(args[++i]);
					}
					catch(NumberFormatException e)
					{
						return false;
					}
					onlyEnglish = false;
					break;
				
				case "-i":
				case "--indent":
					executeIndentCounter = true;
					break;
					
				case "-s":
				case "--switch":
					executeSwitchFinder = true;
					break;
					
				case "-o":
				case "--output":
					if(i+1>args.length)
						return false;
					outputDirectoryName = args[++i];
					break;
					
				default:
					return false;
			}
		}
		
		return true;
	}

}
