package test;

import java.io.File;

import files.T_File;

public class T_FileTest 
{
	private static final String testFileName = "test" + File.separator + "T_FileTestFile.java";
	
	
	public static void main(String[] args)
	{
		
		
		T_File testFile = new T_File(new File(testFileName), null);
		
		int index = testFile.getLineFromIndex(80);
		String line = testFile.getLineWithIndex(80);
		int relativeIndex = testFile.getIndexRelativeToLine(80);
		
		System.out.println(index);
		System.out.print(line);
		System.out.println(relativeIndex);
	}

}
