package results;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class ResultContainer 
{	
	private Writer writer = null;
	
	public ResultContainer(String outputFileName)
	{
		try {
			writer = new OutputStreamWriter(new FileOutputStream(outputFileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void writeResult(String summaryResult)
	{
		try {
			writer.write(summaryResult);
			writer.write("\n\n\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void closeFile()
	{
		try
		{
			writer.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
