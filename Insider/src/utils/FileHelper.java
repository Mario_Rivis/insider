package utils;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class FileHelper {
	
	public static String getExtension(String filename) {
        if (filename == null) {
            return null;
        }
        int extensionPos = filename.lastIndexOf('.');
        int lastUnixPos = filename.lastIndexOf('/');
        int lastWindowsPos = filename.lastIndexOf('\\');
        int lastSeparator = Math.max(lastUnixPos, lastWindowsPos);

        int index = lastSeparator > extensionPos ? -1 : extensionPos;
        if (index == -1) {
            return "";
        } else {
            return filename.substring(index + 1);
        }
    }
	
	public static String getFileNameWithoutExtension(String filename)
	{
		int lastUnixPos = filename.lastIndexOf('/');
        int lastWindowsPos = filename.lastIndexOf('\\');
        int lastSeparator = Math.max(lastUnixPos, lastWindowsPos);
        
        String ret = filename.substring(lastSeparator+1);
        int extensionIndex = ret.lastIndexOf('.');
        
        if(extensionIndex==-1)
        	return ret;
        else
        	return ret.substring(0,extensionIndex);
        
        
	}
	
	public static String getRelativeFileName(File fileName, File relativeTo)
	{
		try {
			return getRelativeFileName(fileName.getCanonicalPath(), relativeTo.getCanonicalPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static String getRelativeFileName(String fileName, String relativeTo)
	{
		if(fileName.indexOf(relativeTo)!=0)
			return "";
	
		String ret = fileName.substring(relativeTo.length()+1);
		
		return ret;
			
	}
	
	public static boolean isBlankLine(String line)
	{
		if(line==null || line.length()==0)
			return true;
		
		char[] letters = line.toCharArray();
		for(int i=0; i<letters.length; i++)
		{
			if(!Character.isWhitespace(letters[i]))
				return false;
		}
		
		return true;
	}
	
	public static String removeComments(String fileContent)
	{
		String ret = fileContent;
		
		Pattern p = Pattern.compile("((\\/\\*([\\S\\s]+?)\\*\\/))");
		Matcher m = p.matcher(ret);
		
		char[] charArray = ret.toCharArray();
		while(m.find())
		{
			int startIndex = m.start();
			int stopIndex = m.end();
			
			for(int i=startIndex+2; i<stopIndex-2; i++)
			{
				if(charArray[i]!='\n'&&charArray[i]!='\t')
					charArray[i] = ' ';
			}
		}
		
		ret = String.valueOf(charArray);
		
		p = Pattern.compile("(?:\\/\\/.*)");
		m = p.matcher(ret);
		
		charArray = ret.toCharArray();
		while(m.find())
		{
			int startIndex = m.start();
			int stopIndex = m.end();
			
			for(int i=startIndex+2; i<stopIndex; i++)
			{
				charArray[i] = ' ';
			}
		}
		
		ret = String.valueOf(charArray);
		
		return ret;
	}
	
}
