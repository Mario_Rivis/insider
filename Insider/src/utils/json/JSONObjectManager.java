package utils.json;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class JSONObjectManager 
{
	private static JSONArray jsonObjects = new JSONArray();
	
	public static void add(JSONObject jsonObject)
	{
		jsonObjects.add(jsonObject);
	}
	
	public static void printToFile(String outputFilePath)
	{
		Writer writer = null;
		try
		{
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFilePath)));
			writer.write(jsonObjects.toJSONString());
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch(IOException e)
			{
				System.out.println("Could not colse file: " + outputFilePath);
				e.printStackTrace();
			}
		}
	}
}
