package utils.json;

import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import files.T_File;
import utils.Property;

public class JsonEncoder {

	public static void encode(T_File file, List<Property> properties)
	{
		for(int i=0; i<properties.size(); i++)
		{
			JSONObject ob = JsonEncoder.encode(properties.get(i));
			ob.put("file", file.getName());
			JSONObjectManager.add(ob);
		}
	}
	
	public static JSONArray encode(String[] words)
	{
		JSONArray wordsArray = new JSONArray();
		
		for(int i=0; i<words.length; i++)
		{
			wordsArray.add(words[i]);
		}
		
		return wordsArray;
	}
	
	private static JSONObject encode(Property p)
	{
		JSONObject ob = new JSONObject();
		
		ob.put("name", p.getName());
		ob.put("value", p.getValue());
		ob.put("category", p.getCategory());
		
		return ob;
	}
}
