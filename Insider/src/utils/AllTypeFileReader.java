package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import features.technlogysearcher.Category;
import features.technlogysearcher.FingerPrint;
import features.technlogysearcher.Technology;
import utils.exceptions.IllegalExtensionException;

public class AllTypeFileReader {

	public static String[] readExtensionsFile(String extensionFilePath) throws IllegalExtensionException
	{
		String fileExtension = FileHelper.getExtension(extensionFilePath);
		if(!fileExtension.equals("extension"))
			throw new IllegalExtensionException("extension", fileExtension);
		
		BufferedReader in = null;
		String line;
		List<String> extensionList = new ArrayList<>();
		try
		{
			in = new BufferedReader(new FileReader(extensionFilePath));
			while((line = in.readLine()) != null)
			{
				if(!FileHelper.isBlankLine(line))
					extensionList.add(line);
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		String extensionsArray[] = new String[extensionList.size()];
		extensionsArray = extensionList.toArray(extensionsArray);
		
		return extensionsArray;
	}
	
	public static String[] readExtensionsFile(File extensionFile) throws IllegalExtensionException
	{
		return readExtensionsFile(extensionFile.getAbsolutePath());
	}
	
	public static Technology readTechnologyFile(String technologyFilePath) throws IllegalExtensionException
	{
		String fileExtension = FileHelper.getExtension(technologyFilePath);
		if(!fileExtension.equals("technology"))
			throw new IllegalExtensionException("technology", fileExtension);
		
		String technologyName = FileHelper.getFileNameWithoutExtension(technologyFilePath);
		Technology technology = new Technology(technologyName);
		
		String categoryName = "Others";
		BufferedReader in = null;
		String line;
		Category c = null;
		
		List<Category> categories = new ArrayList<>();
		List<FingerPrint> fingerPrints = new ArrayList<>();
		
		Category[] catArray = null;
		FingerPrint[] fingerPrintArray = null;
		
		try
		{
			in = new BufferedReader(new FileReader(technologyFilePath));
			line = in.readLine();
			if(line!=null)
			{
				if(!FileHelper.isBlankLine(line))
				{
					if(line.indexOf('(')==0)
					{
						c = new Category(categoryName, technology);
						categories.add(c);
						fingerPrints.add(new FingerPrint(c,line));
					}
					else
					{
						categoryName = line;
						c = new Category(categoryName, technology);
						categories.add(c);
					}
				}
			}
			else
				return technology;
					
			while((line = in.readLine()) != null)
			{
				if(!FileHelper.isBlankLine(line))
				{
					if(line.indexOf('(') != 0)
					{
						fingerPrintArray = new FingerPrint[fingerPrints.size()];
						fingerPrintArray = fingerPrints.toArray(fingerPrintArray);
						c.setFingerPrints(fingerPrintArray);
						fingerPrints.clear();
						
						categoryName = line;
						c = new Category(categoryName, technology);
						categories.add(c);
					}	
					else
					{
						fingerPrints.add(new FingerPrint(c,line));
					}
				}
			}
			
			fingerPrintArray = new FingerPrint[fingerPrints.size()];
			fingerPrintArray = fingerPrints.toArray(fingerPrintArray);
			c.setFingerPrints(fingerPrintArray);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		catArray = new Category[categories.size()];
		catArray = categories.toArray(catArray);
		
		technology.setCategories(catArray);
		
		return technology;
	}
	
	public static Technology readTechnologyFile(File technologyFile) throws IllegalExtensionException
	{
		return readTechnologyFile(technologyFile.getAbsolutePath());
	}
}
