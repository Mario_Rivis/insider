package utils.exceptions;

public class IllegalExtensionException extends RuntimeException 
{

	private String extensionDemanded;
	private String extensionTransmitted;
	
	public IllegalExtensionException(String extensionDemanded, String transmittedExtension)
	{
		this.extensionDemanded = extensionDemanded;
		this.extensionTransmitted = transmittedExtension;
	}
	
	public String toString()
	{
		return "expected extension: " + extensionDemanded + ", extension: " + extensionTransmitted;
	}
}
