package utils;

public class Property {

	private String name;
	private Object value;
	private String category;
	
	public Property(String name, Object value, String category) {
		super();
		this.name = name;
		this.value = value;
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public Object getValue() {
		return value;
	}

	public String getCategory() {
		return category;
	}
	
	
	
}
